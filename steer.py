#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import math
import matplotlib.pyplot as plt
import pygame
import socket ,struct

send_sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
send_sock.bind(("127.0.0.1",40001))

recv_sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)


class steer:
    def __init__(self):
        self.window_height = 600
        self.window_width = 800
        self.haptic = True
        self.K_haptic = 10
        self.K_hand = 10
        self.C = 2 #steer damping
        self.m = 0.1
        self.dt = 0.01  # intergration step timedt = 0.01 # integration step time
        self.dts = self.dt * 1  # desired simulation step time (NOTE: it may not be achieved)
        self.t = 0.0  # time
        self.da = 0  # actual steer velocity
        self.angle = 0
        self.max_angle = 150
        self.haptic_gain = 2


    def steering_angle(self, mouse, road_error):
        cursor_angle = -(mouse - xc) / xc * self.max_angle   # from mouse pixel location to steering angle

        haptic_angle = -road_error[0]/self.window_width*self.max_angle*self.haptic_gain  # transform the car error linear to

        Fhand = self.K_hand * (cursor_angle - self.angle)  # spring force

        Fhaptic = self.K_haptic * (haptic_angle - self.angle) # spring force of the haptic guidance

        if self.haptic:
            F = Fhaptic - self.da * self.C + Fhand
        else:
            F = Fhand - self.da * self.C

        dda = F / self.m
        self.da += dda * self.dt
        self.angle += self.da * self.dt
        self.t += self.dt
        self.angle = np.clip(self.angle, -self.max_angle, self.max_angle)
        color = 255-abs(np.clip(Fhand/10, -255, 255))
        global background
        background = (255, color, color)

    def blitRotate(surf, image, angle):
        pos = (window.get_width() / 2, window.get_height() / 2)
        w, h = steerpng.get_size()
        originPos = (w/2, h/2)
        # calcaulate the axis aligned bounding box of the rotated image
        w, h = image.get_size()
        box = [pygame.math.Vector2(p) for p in [(0, 0), (w, 0), (w, -h), (0, -h)]]
        box_rotate = [p.rotate(angle) for p in box]
        min_box = (min(box_rotate, key=lambda p: p[0])[0], min(box_rotate, key=lambda p: p[1])[1])
        max_box = (max(box_rotate, key=lambda p: p[0])[0], max(box_rotate, key=lambda p: p[1])[1])

        # calculate the translation of the pivot
        pivot = pygame.math.Vector2(originPos[0], -originPos[1])
        pivot_rotate = pivot.rotate(angle)
        pivot_move = pivot_rotate - pivot

        # calculate the upper left origin of the rotated image
        origin = (
            pos[0] - originPos[0] + min_box[0] - pivot_move[0], pos[1] - originPos[1] - max_box[1] + pivot_move[1])

        # get a rotated image
        rotated_image = pygame.transform.rotate(image, angle)

        # rotate and blit the image
        surf.blit(rotated_image, origin)

# SIMULATION
model = steer()

# initialise real-time plot with pygame:
pygame.init() # start pygame
window = pygame.display.set_mode((model.window_width, model.window_height)) # create a window (size in pixels)
window.fill((255,255,255)) # white background
xc, yc = window.get_rect().center # window center
pygame.display.set_caption('steering wheel')

font = pygame.font.Font('freesansbold.ttf', 20) # printing text font and font size
text = font.render('Your task is to steer the car on the lane. press E to start.', True, (0, 0, 0), (255, 255, 255))
text_rect = text.get_rect(center=(window.get_width()/2, window.get_height()/2))
window.blit(text, text_rect)
pygame.display.flip()
clock = pygame.time.Clock() # initialise clock
FPS = int(1/model.dts) # refresh rate

steerpng = pygame.image.load('steer2.png')
steerpng = pygame.transform.scale(steerpng, (400, 400))


# wating for start command:
run = True
while run:
    for event in pygame.event.get(): # interrupt function
        if event.type == pygame.KEYUP:
            if event.key == ord('e'): # enter the main loop after 'e' is pressed
                run = False


# main while loop start with key check to leave the while loop
i = 0
run = True

while run:
    for event in pygame.event.get(): # interrupt function
        if event.type == pygame.QUIT: # force quit with closing the window
            run = False
        elif event.type == pygame.KEYUP:
            if event.key == ord('q'): # force quit with q button
                run = False

    # main control code
    cursor = pygame.mouse.get_pos()

    # # receive road_error:
    msgFromClient = "Hello UDP Server, give me road_error"
    serverAddressPort = ("127.0.0.1", 40002)
    bytesToSend = str.encode(msgFromClient)
    recv_sock.sendto(bytesToSend, serverAddressPort)
    road_error_data, adress = recv_sock.recvfrom(8)  # receive data with buffer size of 8 bytes
    road_error = struct.unpack("=f", road_error_data)  # convert the received data from bytes to array

    # calculate new steer angle:
    model.steering_angle(cursor[0], road_error)

    # send the steer_angle:
    bytesAddressPair = send_sock.recvfrom(8)
    message = bytesAddressPair[0]
    address = bytesAddressPair[1]
    clientMsg = "Message from Client:{}".format(message)
    clientIP = "Client IP Address:{}".format(address)

    send_value = model.angle
    send_data = bytearray(struct.pack("=f", send_value))  # convert array of floats to bytes
    send_sock.sendto(send_data, address)  # send to IP address

    #### visualize everything
    window.fill(background)  # clear window
    model.blitRotate(window, steerpng,  model.angle)
    pygame.display.flip()
    clock.tick(FPS)