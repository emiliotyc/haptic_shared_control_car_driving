import numpy as np
import math
import matplotlib.pyplot as plt
import pygame
import socket ,struct
import csv
from datetime import datetime
import pandas as pd

send_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
send_sock.bind(("127.0.0.1", 40002))

recv_sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)


class simulation:
    def __init__(self):
        self.roadnumber = 4

        #time variables
        self.t = 60 #simulation time
        self.dt = 0.01 #step size
        self.dts = self.dt * 1  # desired simulation step time
        self.steps = int (self.t/self.dt)
        # pygame variables
        self.window_height = 600
        self.window_width = 800
        self.car_width = 70
        self.gray = (100, 100, 100)
        self.lgray = (200, 200, 200)
        self.green = (26, 100, 30)
        #road variables
        self.road_curvature = 70 # road curvature parameter (lower is more sharp curves)
        self.n_sinus = 10 # number of sinusoidals in fourrier road.
        self.x_sinus = np.zeros([int(self.t / self.dt), self.n_sinus])
        self.x_pos_road = np.zeros(int(self.t / self.dt)) # road position
        self.car_height = 0.6*self.window_height
        self.car_location = 0.5*self.window_width
        self.max_angle = 150
        self.pix_per_frame_car_moves = 5# the movement of the car at maximum steering.
        #performance variables
        self.error = np.zeros(self.steps)
        self.steer_angle = np.zeros(self.steps)

        #candidate variables
        self.cadidate_name = 'test'
        self.haptic = True




    def init_road(self):
        np.random.seed(self.roadnumber)
        global freq
        freq = np.random.rand(10)
        for i in range(self.steps):
            self.x_sinus[i] = np.sin(freq*(i-freq)/self.road_curvature)
        self.x_pos_road = self.x_sinus.sum(axis=1)


road = simulation()
road.init_road()

# initialise real-time plot with pygame:
pygame.init()  # start pygame
window = pygame.display.set_mode((road.window_width, road.window_height)) # create a window (size in pixels)
window.fill((255,255,255)) # white background
xc, yc = window.get_rect().center # window center
pygame.display.set_caption('road simulation')

clock = pygame.time.Clock() # initialise clock
FPS = int(1/road.dts) # refresh rate

carpng = pygame.image.load('car.png')  #load car image
carpng = pygame.transform.scale(carpng, (2*road.car_width, road.car_width))  # scale car image
carpng = pygame.transform.rotate(carpng, -90)  # car face north

font = pygame.font.Font('freesansbold.ttf', 20) # printing text font and font size
text_string = 'welcome to this car driving experiment {}'.format(road.cadidate_name)
text = font.render(text_string, True, (0, 0, 0), (255, 255, 255)) # printing text object
text_rect = text.get_rect(center=(window.get_width()/2, window.get_height()/2))
window.blit(text, text_rect)

pygame.display.flip()


run = True
i = 0
while run:
    for event in pygame.event.get(): # interrupt function
        if event.type == pygame.QUIT: # force quit with closing the window
            run = False
        elif event.type == pygame.KEYUP:
            if event.key == ord('q'): # force quit with q button
                run = False

    ###main loop
    cursor = pygame.mouse.get_pos()
    i += 1

    if i == road.steps-1:
        run = False

    i_prev = i - 40  # delay for the road
    i_prev = np.clip(i_prev, 0, 6000)

    x_coordinate = int((road.x_pos_road[i] + road.n_sinus) / (2 * road.n_sinus) * 800)
    x_coordinate_prev = int((road.x_pos_road[i_prev] + road.n_sinus) / (2 * road.n_sinus) * 800)

    a = (x_coordinate - 0.5 * road.car_width, 0)
    b = (x_coordinate + 0.5 * road.car_width, 0)
    c = (x_coordinate_prev + 0.5 * road.car_width, road.window_height)
    d = (x_coordinate_prev - 0.5 * road.car_width, road.window_height)
    correct_car_x_coordinate = (1 - (road.car_height / road.window_height)) * a[0] + \
                               (road.car_height / road.window_height) * d[0]

    road.error[i] = correct_car_x_coordinate - road.car_location


    # send error:
    bytesAddressPair = send_sock.recvfrom(8)
    message = bytesAddressPair[0]
    address = bytesAddressPair[1]
    clientMsg = "Message from road:{}".format(message)
    clientIP = "Client IP Address:{}".format(address)

    send_data = bytearray(struct.pack("=f", road.error[i]))  # convert array of 2 floats to bytes
    send_sock.sendto(send_data, address)  # send

    # receive steer_angle:
    msgFromClient = "Hello steer, give me steer_angle"
    serverAddressPort = ("127.0.0.1", 40001)
    bytesToSend = str.encode(msgFromClient)
    recv_sock.sendto(bytesToSend, serverAddressPort)

    steer_angle_data, address = recv_sock.recvfrom(8)  # receive data with buffer size of 8 bytes
    actual_steer_angle = struct.unpack("=f", steer_angle_data)  # convert the received data from bytes to float
    road.steer_angle[i] = actual_steer_angle[0]

    #calculate new car_position
    road.car_location = road.car_location - road.steer_angle[i]/road.max_angle*road.pix_per_frame_car_moves
    road.car_location = np.clip(road.car_location,0,road.window_width-road.car_width)

    #### visualize everything
    window.fill(road.green)  # clear window
    pygame.draw.polygon(window, road.lgray, ((a[0] - 2, a[1]), (b[0] + 2, b[1]), (c[0] + 2, c[1]), (d[0] - 2, d[1])))
    pygame.draw.polygon(window, road.gray, (a,b,c,d))
    window.blit(carpng, [int(road.car_location), road.car_height])
    pygame.display.flip()
    clock.tick(FPS)

dateTimeObj = datetime.now()
timestampStr = dateTimeObj.strftime("%d-%b-%Y_%H:%M:%S")
if road.haptic:
    csv_name_string = 'results/results_of_{}_with_haptic_{}.csv'.format(road.cadidate_name, timestampStr)
    png_name_string = 'results/results_of_{}_with_haptic_{}.png'.format(road.cadidate_name, timestampStr)
else:
    csv_name_string = 'results/results_of_{}_without_haptic_{}.csv'.format(road.cadidate_name, timestampStr)
    png_name_string = 'results/results_of_{}_without_haptic_{}.png'.format(road.cadidate_name, timestampStr)


df = pd.DataFrame({'error:': road.error, 'angle:': road.steer_angle, 'x-pos of the road:': road.x_pos_road })
df.to_csv(csv_name_string)

plt.figure()
plt.plot(road.error)

plt.title("Steering error")

plt.ylabel("pix [# pixels]")
plt.savefig(png_name_string, dpi=400)
plt.show()

print('your score is:{}'.format(1/np.sum(np.abs(road.error))*10000000))

# with open(csv_name_string, mode='w') as employee_file:
#     employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
#
#     employee_writer.writerow(['error', 'Accounting', 'November'])
#     employee_writer.write(error)